# CAD
CAD (computational ataxia diagnosis) is a program that can be used to classify patients with ataxia using data from the 
finger chase test. CAD calculates first different attributes such as the power spectrum, residuals, the jerk and the peaks
in the data. After the calculation of the attributes an random tree forest is trained on the data set and can be used to
classify an unknown patient

## Usage
There are two different usage of CAD. Each usage is divided in two steps.   
One is the training step in which the algorithm is trained on a given training set.   
And the next step is the predicting step in which a saved trained model
is used to classify an unknown patient.  

CAD can use files in the following format:  

| 05:00:39  | -0.36972  | -1.78315  | 0.286682   |        
| 05:00:39  | -0.37471  | -1.77462  | 0.291946   |   
| 05:00:39  | -0.38457  | -1.76501  | 0.293530   |        
| …        | …         | …         | …          | 

The first column contains the timestamp of the measurement.   
The second column contains the measurement on the x axis.  
The third on the y axis.
And the fourth on the z axis.

The information table CAD uses to extract information for the subjects, contains five columns.  
The first column contains the id of the subject. The second contains the age of the subject.  
The third contains the gender of the subject. The fourth contains if a subject is sick or healthy.   
The last column describes the clinical picture of the subject.

#### Command line version:
The usage of the command line version of CAD.
usage: 
```
machine.py [-h] [-t TRAINING_DATA_DIRECTORY]
                  [-i SAMPLE_INFORMATION_FILE_PATH] [-o OUTPUT_TABLE]
                  [-m MODEL_PATH] [-n NEW_SUBJECT_PATH] [-r REPEATS]

Train machine learning model

optional arguments:
  -h, --help            show this help message and exit
  -t TRAINING_DATA_DIRECTORY, --training_data_directory TRAINING_DATA_DIRECTORY
                        Give a path for the directory where subject folders
                        are stored
  -i SAMPLE_INFORMATION_FILE_PATH, --sample_information_file_path SAMPLE_INFORMATION_FILE_PATH
                        Specify a CSV file that contains subject folder names
                        (at column 1) and labels (at column number 4)
  -o OUTPUT_TABLE, --output_table OUTPUT_TABLE
                        Specify the path for the output CSV file
  -m MODEL_PATH, --model_path MODEL_PATH
                        The path where the input model should be stored
  -n NEW_SUBJECT_PATH, --new_subject_path NEW_SUBJECT_PATH
                        A path to a subject that needs to be classified
  -r REPEATS, --repeats REPEATS
                        The number of times to repeat cross validation for a
                        reliable score
```

To evaluate an algorithm on a given training set you can use the following command:
> python3 machine.py -t TRAIN_DIRECTORY -i SAMPLE_INFORMATION_FILE_PATH [-r | --repeats 1]

The repeats option can be used to get a more reliable average score from the cross-validation

To train an algorithm on a given training set you have to supply a path the model is saved to in addition to the arguments required for evaluation, as in the following command:
> python3 machine.py -t TRAIN_DIRECTORY -i SAMPLE_INFORMATION_FILE_PATH -m MODEL_PATH [-r | --repeats 1]

To retrieve the processed data you have to supply a path the output table is saved to in addition to the arguments required for evaluation, as in the following command:
> python3 machine.py -t TRAIN_DIRECTORY -i SAMPLE_INFORMATION_FILE_PATH -o OUTPUT_TABLE

To predict if a patient has ataxia you can use the command:
> python3 machine.py -m MODEL_PATH -n NEW_SUBJECT_PATH

The outputs are described within the [GUI description](####Graphical-user-interface-version)

##### Parameter estimator & Power plot

To plot the the magnitudes of a small range of frequencies from the Fourier transformation use the following command
> python3 power_plot.py -t TRAIN_DIRECTORY -i SAMPLE_INFORMATION_FILE_PATH

A similar command can be used to perform parameter estimation for the random forest classifier
> python3 parameter_estimator.py -t TRAIN_DIRECTORY -i SAMPLE_INFORMATION_FILE_PATH

In the parameter_estimator.py file the parameter search ranges can be altered.
Each parameter can be tested with a collection of possible values like below:
```python
# Define some search ranges for various attributes
parameter_search_ranges = {'max_features': range(3, 10, 1),
                           'max_depth': range(2, 10, 1),
                           'min_samples_split': np.arange(0.001, 0.002, 0.0001),
                           'min_samples_leaf': np.arange(0.18, 0.20, 0.001),
                           'max_leaf_nodes': range(120, 140, 1),
                           'min_impurity_split': np.arange(0.18, 0.20, 0.001)}
```

#### Graphical user interface version:
Next to the command line version there is also an user friendly graphic user interface available.  
The graphic user interface can be started command line with the command:
> python3 GUI.py

If you start the application you will get the following screen:     
![Application screen shot](screenshots/Knipsel1.PNG)

In this screen you can see two tabs train and predict.  
These two steps contain the same steps described at the commandline version training and predict.
The first tab selected is train. Here you can train an random forest on a given data set.  
Non optional arguments are the training data set and the information of the patients.    
The information of the patients is used to extract if a subject is healthy or has ataxia or DCD.   
If you have inserted all the needed arguments, you can hit the button commit and train.  
This will train the algorithm on the given data set.  

After training you will get this results screen:     
![Application train results](screenshots/Knipsel2.PNG)

In this screen shot you can see some warnings shown in red.  
These warnings contain information of subjects that were not found in the data set.

After you have saved the model you have trained, you can click on the predict tab.  
In the predict tab you can give two arguments.  
The path to the model you have trained and the path to the subject you want to classify.  
If you have inserted the arguments and hit the predict button.  

This screen will be shown:   
![Application predict results](screenshots/Knipsel3.PNG)

In this results you can see the name of the subject you want to classify, the type in which the subject was classified  
and the accuracy of the classification.
