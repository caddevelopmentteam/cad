#!/usr/bin/env python3
"""
Graphic user interface for a application to automatic classify ataxia patients
"""

import contextlib
import io
import threading
import tkinter as tk
from tkinter import ttk, messagebox, filedialog

import joblib

from src.machine import *


class StdRedirector(io.StringIO):
    """
    Object responsible for handling writing to a given ui widget
    """
    def __init__(self, widget, tag=None):
        """
        Constructor method
        :param widget: widget to write to
        :param tag: string representing a special tag for the given widget
        """
        super().__init__()
        self.tag = tag
        self.widget = widget

    def write(self, string):
        """
        Method writing a string
        :param string: what to write
        """
        super().write(string)
        # Set widget to normal state
        self.widget.config(state=tk.NORMAL)
        # Insert using a tag or without
        if self.tag:
            self.widget.insert(tk.END, string, self.tag)
        else:
            self.widget.insert(tk.END, string)
        # Set widget to disabled state
        self.widget.config(state=tk.DISABLED)
        self.widget.see(tk.END)


class Tab(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.result_field = None
        self.messages = list()
        self.path = None
        return

    def create_result_field(self):
        """
        Creates a text box where on the results can be printed.
        :return:
        """
        self.result_field = tk.Text(self)
        scrollbar = tk.Scrollbar(self, command=self.result_field.yview, orient='vertical')
        self.result_field.config(state=tk.DISABLED)
        self.result_field.tag_config("warning", foreground="red")
        self.result_field.grid(row=2, column=3, rowspan=10)
        scrollbar.grid(row=2, column=4, rowspan=10)
        self.result_field['yscrollcommand'] = scrollbar.set
        return

    def print_results_on_results_field(self, results, type_print):
        """
        Prints the results in the text box
        :param results:
        :return:
        """
        # Ables input in the text box
        self.result_field.config(state=tk.NORMAL)
        self.result_field.insert(tk.END, results, type_print)
        # Disables input in the text box
        self.result_field.config(state=tk.DISABLED)
        return

    def give_messages(self):
        """
        Shows if there are error messages, error messages to the user.
        :return:
        """
        for message in self.messages:
            messagebox.showerror(title="Error", message=message)
        self.messages = list()
        return

    def browse_directories(self):
        """
        Browses directories for a certain directory
        :return:
        """
        self.path = filedialog.askdirectory()

        return

    def browse_files(self):
        """
        Browses files for a certain file
        :return:
        """
        self.path = filedialog.askopenfilename()
        return


class Train(Tab):
    """
    Train is a class that inherits from Tab. And contains an tab that can train an algorithm on a given data set.
    """

    def __init__(self, parent):
        super().__init__(parent)

        self.number_repetitions = None
        self.training_directory = None
        self.information_table = None
        self.output_csv = None
        self.model_path = None
        self.path = None

        self.training_directory_entry = tk.Entry(self)
        self.information_table_entry = tk.Entry(self)
        self.output_csv_entry = tk.Entry(self)
        self.model_path_entry = tk.Entry(self)
        self.number_repetitions_entry = tk.Entry(self)

        self.create_widgets()
        return

    def create_widgets(self):
        """
        Creates the widgets of the tab Train
        :return:
        """
        # Describe the usage of the tab
        self.describe_usage()

        # Entry for training directory
        lbl = tk.Label(self, text="* Insert the training directory:")
        lbl.grid(row=1, column=0)
        self.training_directory_entry.grid(row=2, column=0)
        browse_button = tk.Button(self, text="browse", command=self.browse_for_training_dir)
        browse_button.grid(row=2, column=1, sticky=tk.W)

        # Entry for information_csv
        lbl = tk.Label(self, text="* Insert an information csv file:")
        lbl.grid(row=3, column=0)
        self.information_table_entry.grid(row=4, column=0)
        browse_button = tk.Button(self, text="browse", command=self.browse_for_information_table)
        browse_button.grid(row=4, column=1, sticky=tk.W)

        # Entry for output_csv
        lbl = tk.Label(self, text="Insert a path and an filename for an output csv file:")
        lbl.grid(row=5, column=0)
        self.output_csv_entry = tk.Entry(self)
        self.output_csv_entry.grid(row=6, column=0)

        # Entry for output model
        lbl = tk.Label(self, text="Insert a path to save the model:")
        lbl.grid(row=7, column=0)
        self.model_path_entry = tk.Entry(self)
        self.model_path_entry.grid(row=8, column=0)

        # Entry for number of repetitions
        lbl = tk.Label(self, text="Insert number of repetitions. Default is ten")
        lbl.grid(row=9, column=0)
        self.number_repetitions_entry.grid(row=10, column=0)
        self.number_repetitions_entry.insert(1, 10)

        # create result field
        self.create_result_field()

        # Train button
        browse_button = tk.Button(self, text="Commit and train", command=self.start_training)
        browse_button.grid(row=11, column=0)
        return

    def describe_usage(self):
        """
        Describes the usage of the tab Train
        :return:
        """
        lbl = tk.Label(self,
                       text="Here you can train an algorithm on a given data set.\nAlso you can save the model to a "
                            "given file location.\nArguments with * are non optional\n")
        lbl.grid(row=0, column=0, columnspan=2)
        return

    def browse_for_training_dir(self):
        """
        Browses for the training directory
        :return:
        """
        self.browse_directories()
        self.training_directory_entry.insert(1, self.path)
        return

    def browse_for_information_table(self):
        """
        Browses for the information table csv file
        :return:
        """
        self.browse_files()
        self.information_table_entry.insert(1, self.path)
        return

    def start_training(self):
        """
        Trains an algorithm on a given data set.
        :return:
        """
        # Checks the input given by the user.
        self.check()
        # If there are some errors give them to the user.
        if self.messages:
            self.give_messages()
        else:
            training_thread = threading.Thread(
                target=self.train)
            training_thread.start()
        return

    def train(self):
        with stdout_redirect(StdRedirector(self.result_field)), stderr_redirect(
                StdRedirector(self.result_field, "warning")):
            patient_information = get_sample_information_table(self.information_table)
            machine = Machine(self.training_directory, patient_information)
            clf = get_estimator_instance()
            # Evaluate the classifier with the data in args.
            machine.evaluate(clf, get_cross_validation_indices(), self.number_repetitions)
            # Process output options.
            if self.output_csv:
                machine.save_output_csv(self.output_csv)
            # Train estimator.
            machine.train(clf)
            if self.model_path:
                joblib.dump(machine, self.model_path)

    def check(self):
        """
        Checks the input given by the user
        :return:
        """
        # Gets the input of the user
        training_directory = self.training_directory_entry.get()
        information_table = self.information_table_entry.get()
        output_csv = self.output_csv_entry.get()
        model_path = self.model_path_entry.get()
        number_of_repetitions = self.number_repetitions_entry.get()

        # Checks the variable training directory
        if not os.path.isdir(training_directory):
            self.messages.append("You have given a wrong directory")
        else:
            self.training_directory = training_directory

        # Checks the variable information table
        if not os.path.isfile(information_table):
            self.messages.append("You have given a wrong file for the information table")
        else:
            self.information_table = information_table

        # Checks the variable output csv file
        if output_csv:
            csv_output_dir = os.path.dirname(output_csv)
            if not os.path.isdir(csv_output_dir):
                self.messages.append("You have given a wrong path for an output file")
            else:
                self.output_csv = output_csv

        # Checks the variable model path
        if model_path:
            model_path_dir = os.path.dirname(model_path)
            if not os.path.isdir(model_path_dir):
                self.messages.append("You have given a wrong path to save the model")
            else:
                self.model_path = model_path

        # Checks if number of repetitions is an integer and not negative or null
        try:
            number_of_repetitions = int(number_of_repetitions)
        except ValueError:
            self.messages.append("Number of repetitions must be an integer")
            return
        if number_of_repetitions == 0:
            self.messages.append("Number of repetitions can not be zero")
        else:
            if number_of_repetitions < 0:
                self.messages.append("You have given an negative number of repetitions")
            else:
                self.number_repetitions = number_of_repetitions
        return


class Predict(Tab):
    def __init__(self, parent):
        super().__init__(parent)
        self.file_patient_entry = tk.Entry(self)
        self.file_model_entry = tk.Entry(self)
        self.create_widgets()
        self.model_path = None
        self.new_subject_path = None
        return

    def describe_usage(self):
        """
        Gives a description of the usage of the tab Predict
        :return:
        """
        lbl = tk.Label(self, text="Here you can predict if an unknown patient\n has Ataxia or not with a given model.\n"
                                  "Arguments with * are non optional")
        lbl.grid(row=0, column=0, columnspan=2)
        return

    def create_widgets(self):
        """
        Creates the widgets for tab Predict
        :return:
        """
        self.describe_usage()
        # Input for not known patient file
        lbl = tk.Label(self, text="* The patient file")
        lbl.grid(row=1, column=0)
        self.file_patient_entry.grid(row=2, column=0)
        browse_button = tk.Button(self, text="browse", command=self.browse_files_patient)
        browse_button.grid(row=2, column=1)

        # Input for path to model
        lbl = tk.Label(self, text="* Path to model")
        lbl.grid(row=3, column=0)
        self.file_model_entry.grid(row=4, column=0)
        browse_button = tk.Button(self, text="browse", command=self.browse_files_model)
        browse_button.grid(row=4, column=1)

        # results field
        self.create_result_field()

        # predict button
        predict_button = tk.Button(self, text="predict", command=self.start_predicting)
        predict_button.grid(row=5, column=0)
        return

    def browse_files_patient(self):
        """
        Browses for a subject file.
        :return:
        """
        self.browse_directories()
        self.file_patient_entry.insert(1, self.path)
        return

    def browse_files_model(self):
        """
        Browses for a model file.
        :return:
        """
        self.browse_files()
        self.file_model_entry.insert(1, self.path)
        return

    def start_predicting(self):
        """
        Predicts for a given subject if the subject is healthy, has ataxia or has DCD.
        :return:
        """
        # Checks if the given input by the user is correct
        self.check()
        # If there are some errors show them to the user
        if self.messages:
            self.give_messages()
        else:
            training_thread = threading.Thread(
                target=self.predict)
            training_thread.start()
        return

    def predict(self):
        with stdout_redirect(StdRedirector(self.result_field)), stderr_redirect(
                StdRedirector(self.result_field, "warning")):
            # Loads the model
            machine = joblib.load(self.model_path)
            # Creates a subject from the inserted path
            subject = Subject.create_subject(self.new_subject_path)
            print(machine.predict_subject(subject))

    def check(self):
        """
        Checks if the input given by the user is error proof.
        :return:
        """
        # Gets the input
        model_path = self.file_model_entry.get()
        subject_path = self.file_patient_entry.get()
        # Checks the path for the model
        if os.path.isfile(model_path):
            self.model_path = model_path
        else:
            self.messages.append("You have given a wrong model file")
        # Checks the path for the subject
        if os.path.isdir(subject_path):
            self.new_subject_path = subject_path
        else:
            self.messages.append("You have given a wrong subject directory")
        return


class CadApplication(tk.Frame):
    """
    A class that inherits from Frame. And contains a notebook which contains two tabs train and predict.
    """

    def __init__(self, master):
        super().__init__(master)
        self.pack()
        self.master.title("CAD")
        self.create_widgets()
        return

    def create_widgets(self):
        """
        Creates the widgets in the application
        :return:
        """
        # creates a note book
        nb = ttk.Notebook(self)
        nb.pack(fill='both', expand='yes')
        # creates the multiple tabs
        train = Train(nb.master)
        predict = Predict(nb.master)
        nb.add(train, text="Train")
        nb.add(predict, text="Predict")
        return


@contextlib.contextmanager
def stdout_redirect(where):
    """
    Context manager that sets the standard out to where while in the context
    :param where: an object instance with a write method
    """
    sys.stdout = where
    try:
        yield where
    finally:
        sys.stdout = sys.__stdout__


@contextlib.contextmanager
def stderr_redirect(where):
    """
    Context manager that sets the standard error to where while in the context
    :param where: an object instance with a write method
    """
    sys.stderr = where
    try:
        yield where
    finally:
        sys.stderr = sys.__stderr__


def main():
    root = tk.Tk()
    gui = CadApplication(root)
    root.mainloop()
    return


if __name__ == '__main__':
    sys.exit(main())
