#!/usr/bin/env python3

"""
Automatic classification system for detecting movement disorders through the finger chase exercise
"""

import argparse
import os
import sys

import sklearn
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.base import clone
from sklearn.dummy import DummyClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.model_selection import LeaveOneOut
from src.subject import DirNotFoundError
from src.subject import Subject
from src.subject import SubjectType
from src.arm import ArmFileNotFoundError

__author__ = "Darius Dubber & Robert Warmerdam"
__version__ = "0.1"


class Machine:
    """
    Object responsible for training and evaluating a classifier
    """
    project_version = __version__
    sklearn_version = sklearn.__version__
    subjects = np.array([])
    _feature_names = np.array([])
    _label_array = np.array([])
    _feature_array = None
    _fitted_estimator_instance = None

    def __init__(self, path, sample_information_table):
        self.repeated_cross_validation_summary = RepeatedCrossValidationSummary([])
        self.training_subjects_path = path
        self._gather_subjects(sample_information_table)
        self._get_feature_arrays()

    def _gather_subjects(self, sample_information_table):
        """
        Gathers features of different participants
        :param sample_information_table: A numpy array with a table that contains information about all subjects
        """
        entries = os.listdir(self.training_subjects_path)
        for entry in entries:
            path_to_subject = os.path.join(self.training_subjects_path, entry)
            try:
                subject = Subject.create_subject(path_to_subject)
                rows = [i for i, c in enumerate(sample_information_table) if subject.name.endswith(str(c[0]))]
                if len(rows) == 1:
                    subject_type = sample_information_table[rows[0]][4]
                    if subject_type != SubjectType.UNKNOWN:
                        subject.subject_type = subject_type
                        self.subjects = np.append(self.subjects, subject)
                    else:
                        raise OmittingSubjectWarning(
                            "subject '{}' has unknown condition and will be omitted from training".format(
                                subject.name)
                        )
                elif len(rows) == 0:
                    raise OmittingSubjectWarning(
                        "subject '{}' can't be found in the information table and will be omitted from training".format(
                            subject.name))
            except (ArmFileNotFoundError, DirNotFoundError, OmittingSubjectWarning) as e:
                sys.stderr.write(str(e) + os.linesep)
            except (Exception) as e:
                error_format_string = "an exception occurred while handling subject '{0}':{linesep}{1}"
                sys.stderr.write(error_format_string.format(entry, str(e), linesep=os.linesep))

    def _get_feature_arrays(self):
        """
        Method collecting feature arrays from subjects
        """
        if len(self.subjects) != 0:
            # Initialize feature_names and feature_array with the right column count
            self._feature_names = sorted(list(self.subjects[0].get_np_feature_array().keys()), reverse=True)
            self._feature_array = np.empty(shape=len(self._feature_names))
        for subject in self.subjects:
            # Add subject feature row
            features = subject.get_np_feature_array()
            subject_ft_arr = np.asarray([features[key] for key in self._feature_names])
            self._feature_array = np.vstack((self._feature_array, subject_ft_arr))
            # Add subject type
            self._label_array = np.append(self._label_array, str(subject.subject_type))
        self._feature_array = np.delete(self._feature_array, 0, 0)

    def _output_zero_rule_score(self):
        """
        Method printing the zero R score
        """
        # Initialize zero r classifier and perform a fit
        zr = DummyClassifier(strategy='most_frequent', random_state=0)
        zr.fit(self._feature_array, self._label_array)
        # Output score
        zero_r_score = zr.score(self._feature_array, self._label_array)
        print("zeroR accuracy = {}\n".format(zero_r_score))

    def evaluate(self, clf, cv, number_of_repetitions):
        # Zero R
        """
        Method evaluating a classifier using cross validation indexes and a given number of repetitions
        :param clf: Sklearn classifier
        :param cv: cross validation indexes
        :param number_of_repetitions: the number of times to repeat the cross validation
        """
        self._output_zero_rule_score()
        # Get cross validation indices
        cross_validation_indices = cv
        # evaluate
        self._eval(clf, cross_validation_indices, number_of_repetitions)

    def _eval(self, cross_validation_indices, estimator_instance, n):
        """
        Performs training of an estimator instance (model) that is given
        :param cross_validation_indices: How splitting of test/training set should be done
        :param estimator_instance: An estimator instance, or model
        :param n: the amount of times to repeat the training to calculate a stable average
        :return: The average score of the training
        """
        # Empty scores
        summaries = list()
        for i in range(n):
            # Perform cross validation using the model and cross validation indices
            summary = self.cross_validate(clf=cross_validation_indices, skf=estimator_instance)
            # Print the summary
            print(summary)
            summaries.append(summary)
        # Store summaries in summary object
        self.repeated_cross_validation_summary = RepeatedCrossValidationSummary(summaries)
        print(self.repeated_cross_validation_summary)
        return self.repeated_cross_validation_summary.get_average_cross_validation_score()

    def train(self, estimator_instance):
        """
        Train the an estimator instance on the entire data set
        :param estimator_instance: an estimator instance from the sklearn package
        """
        estimator_instance_clone = clone(estimator_instance)
        estimator_instance_clone.fit(self._feature_array, self._label_array)
        self._fitted_estimator_instance = estimator_instance_clone

    def cross_validate(self, clf, skf):
        """
        Perform cross validation given a classifier, feature array, label array and stratified K fold.
        :param clf: classifier
        :param skf: stratified k fold object
        """
        feature_array = self._feature_array
        label_array = self._label_array
        predicted_labels_labels_per_predicted_instances = list()
        true_labels_per_predicted_instances = list()
        feature_importances = list()
        for i, indices in enumerate(skf.split(feature_array, label_array)):
            train_index, test_index = indices
            clone_clf = clone(clf)
            # Get different arrays for testing and training
            f_train, f_test = feature_array[train_index], feature_array[test_index]
            l_train, l_test = label_array[train_index], label_array[test_index]
            # Fit to the model
            clone_clf.fit(f_train, l_train)
            [feature_importances.append(tree.feature_importances_) for tree in clone_clf.estimators_]
            # Do predict test data
            predicted_labels = clone_clf.predict(f_test)
            predicted_labels_labels_per_predicted_instances.extend(predicted_labels)
            true_labels_per_predicted_instances.extend(l_test)
        matrix = np.array(feature_importances)
        # Calculate the average of importances along observations (axis 0)
        mean_of_importances = np.average(matrix, axis=0)
        # Standard error of importances along observations (axis 0
        stderr_of_importances = np.std(matrix, axis=0)
        return CrossValidationEvalSummary(true_labels_per_predicted_instances,
                                          predicted_labels_labels_per_predicted_instances,
                                          self._feature_names,
                                          mean_of_importances,
                                          stderr_of_importances)

    def filter_attributes(self):
        """
        method filtering attributes
        The CFS subset evaluator present in weka gave the best results
        Results showed the features:
        """
        i = np.where(self._feature_names not in (
            "min.upper_arm.residuals_mean", "min.upper_arm.residuals_kurtosis", "min.upper_arm.jerk_variance",
            "min.hand.residuals_mean", "min.hand.jerk_skewness", "min.forearm.jerk_skewness", "max.upper_arm.jerk_mean",
            "max.upper_arm.duration", "max.hand.residuals_mean", "max.forearm.residuals_max"))
        self._feature_names = np.delete(self._feature_names, i, 0)
        self._feature_array = np.delete(self._feature_array, i, 1)

    def predict_subject(self, subject):
        """
        Method predicting a subject
        :param subject: A subject object instance
        :return: Prediction summary object instance
        """
        subject_ft_arr = np.asarray([subject.get_np_feature_array()[key] for key in self._feature_names])
        reshaped_feature_array = subject_ft_arr.reshape(1, -1)
        probability = self._fitted_estimator_instance.predict_proba(reshaped_feature_array)[0]
        predicted_subject_type = self._fitted_estimator_instance.predict(reshaped_feature_array)[0]
        return PredictionSummary(subject, predicted_subject_type, probability)

    def save_output_csv(self, output_table_path):
        """
        Saves the features with names to a csv file with a given name
        :param output_table_path: The path to write the output to
        """
        # Combine label array and feature array so the columns get a header
        out = np.c_[self._label_array, self._feature_array]
        # Save the combined 2d array and save this as a csv to the given path
        np.savetxt(output_table_path, out, delimiter=",", header="pat," + ",".join(self._feature_names), fmt='%s')


class OmittingSubjectWarning(UserWarning):
    pass


class PredictionSummary:
    """
    Object summarizing a prediction of a subject
    """

    def __init__(self, subject, predicted_subject_type, probability):
        """
        Constructor method
        :param subject: A subject object instance
        :param predicted_subject_type: The predicted subject type
        :param probability: The probability of the subject type
        """
        self.subject = subject
        self.probability = probability
        self.predicted_subject_type = predicted_subject_type

    def __str__(self):
        """
        Method formatting the various attributes into a string representation for the object instances
        :return: A string representation of object instances
        """
        metric_format_str = "{{:40}}: {{}}{linesep}".format(linesep=os.linesep)
        prediction_metrics = [metric_format_str.format("Subject name", self.subject.name),
                              metric_format_str.format("Predicted class", self.predicted_subject_type),
                              metric_format_str.format("Probability", self.probability)]
        return "".join(prediction_metrics)


class RepeatedCrossValidationSummary:
    """
    Object summarizing multiple cross validated classifications
    """

    def __init__(self, classification_summaries):
        """
        Constructor method
        :param classification_summaries: list of cross validation summaries
        """
        self._summaries = classification_summaries

    def get_average_cross_validation_score(self):
        """
        Method calculating the average cross validation accuracy score
        :return: average cross validation accuracy
        """
        return np.average(self.get_accuracies_per_summary())

    def get_min_cross_validation_score(self):
        """
        Method calculating the minimum cross validation accuracy
        :return: minimum cross validation accuracy
        """
        return np.min(self.get_accuracies_per_summary())

    def get_max_cross_validation_score(self):
        """
        Method calculating the maximum cross validation accuracy
        :return: maximum cross validation accuracy
        """
        return np.max(self.get_accuracies_per_summary())

    def get_accuracies_per_summary(self):
        """
        Method collecting the accuracies of the cross validation summaries
        :return: List of cross validation accuracies
        """
        return [summary.accuracy_score() for summary in self._summaries]

    def format_repeat_summary_with_header(self, i, summary):
        """
        Method formatting a cross validation summary with a header
        :param i: Number of the summary
        :param summary: Cross validation summary object instance
        :return: string representation of hte cross validation summary with a header
        """
        # Declare format
        repeat_header_format_string = 'CROSS VALIDATION REPEAT {0} of {1}{linesep}{linesep}{2}{linesep}'
        # Return formatted string
        return repeat_header_format_string.format(i + 1, len(self._summaries), str(summary), linesep=os.linesep)

    def __str__(self):
        """
        Method converting object instance to string
        :return: String representation of object instance
        """
        # Declare format
        score_string_format = ["Average classification score: {0}",
                               "Minimum classification score: {1}",
                               "Maximum classification score: {2}"]
        # Return formatted string
        return os.linesep.join(score_string_format).format(
            self.get_average_cross_validation_score(), self.get_min_cross_validation_score(),
            self.get_max_cross_validation_score())


class CrossValidationEvalSummary:
    """
    Object summarizing the classification of test data
    """

    def __init__(self, true_labels, predicted_labels, feature_names, mean_of_importances, stderr_of_importances):
        # Set feature names
        self.feature_names = feature_names
        # Set standard error of feature importances
        self.stderr_of_importances = stderr_of_importances
        # Set mean of feature importances
        self.mean_of_importances = mean_of_importances
        # Set predicted labels
        self.predicted_labels = predicted_labels
        # Set true labels
        self.true_labels = true_labels

    def correctly_classified_instances(self):
        """
        Method calculating the amount of correctly classified instances
        :return: the amount of correctly classified instances
        """
        # if the predicted label equals the true label the instance needs to be counted
        return sum([1 if i == x else 0 for i, x in zip(self.predicted_labels, self.true_labels)])

    def incorrectly_classified_instances(self):
        """
        Method calculating the amount fo incorrectly classified instances
        :return: the amount of incorrectly classified instances
        """
        # Teh amount of incorrect predictions equals
        # the total number of predictions minus the amount of correct predictions
        return self.total_number_of_instances() - self.correctly_classified_instances()

    def confusion_matrix(self):
        """
        Method producing a labeled confusion matrix
        :return: Labeled confusion matrix formatted to string
        """
        # Get the confusion matrix
        cm = metrics.confusion_matrix(self.true_labels, self.predicted_labels)
        # Get the maximum amount of digits
        length_of_longest_str = max([max([len(str(item)) for item in row]) for row in cm])
        # Format column headers
        labels = sorted(set(self.true_labels))
        # Add a,b,c,... as labels to the headers
        # chr(i + 97) converts i = 0, 1, 2, ... to 97, 98, 99, ... to a, b, c, ... with chr()
        chars = [chr(i + 97) for i, labels in enumerate(labels)]
        # Align the contents of the table to the right, filling the leftover space with whitespace
        header = ' '.join([str(item).rjust(length_of_longest_str) for item in chars] + ['<- classified as'])
        # Declare lambda with list comprehension for justifying items in a row
        justify_cells = lambda row: [str(item).rjust(length_of_longest_str) for item in row]
        # Declare lambda for making lists with row labels
        row_labels = lambda i: ['|', chars[i], '=', labels[i]]
        # Format matrix
        formatted_mat = ('\n'.join([' '.join(justify_cells(row) + row_labels(i)) for i, row in enumerate(cm)]))
        # Include header
        return header + os.linesep + formatted_mat

    def cohen_kappa_score(self):
        """
        Method calling cohen_kappa_score function from sklearn returning the cohen kappa score
        :return: cohen kappa score
        """
        return metrics.cohen_kappa_score(self.true_labels, self.predicted_labels)

    def accuracy_score(self):
        """
        Method calling the accuracy_score function from sklearn returning the accuracy score (fraction of correctly
        classified instances)
        :return: accuracy score
        """
        return metrics.accuracy_score(self.true_labels, self.predicted_labels)

    def total_number_of_instances(self):
        """
        Method returning the total number of instances calculated with the len() builtin function
        :return: total number of predicted instances
        """
        return len(self.true_labels)

    @staticmethod
    def format_metric(header, score, bracket_value=None):
        """
        Static method formatting a cross validation metric
        :param header: The name of the metric
        :param score: The metrics output
        :param bracket_value: A possible second metric output
        :return: formatted metric name: value string
        """
        if not bracket_value:
            return "{:40}: {}".format(header, score)
        else:
            return "{:40}: {} ({})".format(header, score, bracket_value)

    def __str__(self):
        """
        Method converting object instances to a string
        :return: string representation of object instance
        """
        # List top feature importances
        indices = np.argsort(self.mean_of_importances)[::-1]
        feature_importance_string_list = list()
        for f in range(min(self.mean_of_importances.size, 10)):
            feature_importance_string_list.append(
                "{}. {:<40} {:.4f} (std.err: {:.4f})".format(f + 1,
                                                             self.feature_names[indices[f]],
                                                             self.mean_of_importances[indices[f]],
                                                             self.stderr_of_importances[indices[f]]))

        # List metrics
        classification_metrics = [
            self.format_metric("Correctly classified instances (%)", self.correctly_classified_instances(),
                               self.accuracy_score()),
            self.format_metric("Incorrectly classified instances (%)", self.incorrectly_classified_instances(),
                               (1 - self.accuracy_score())),
            self.format_metric("Kappa statistic", self.cohen_kappa_score()),
            self.format_metric("Total number of instances", self.total_number_of_instances())]

        # Concatenate metric list with confusion matrix
        return os.linesep.join(["CROSS VALIDATION OVERVIEW",
                                self.summary_paragraph("Top 10 feature importance",
                                                       os.linesep.join(feature_importance_string_list)),
                                self.summary_paragraph("Validation summary",
                                                       os.linesep.join(classification_metrics)),
                                self.summary_paragraph("Confusion matrix", self.confusion_matrix())])

    @staticmethod
    def summary_paragraph(header, content):
        """
        Format paragraph for string representation of object instance
        :param header: Name of the paragraph
        :param content: The content of the paragraph
        :return: Formatted paragraph
        """
        # format paragraph with the right line separator
        return "{0}{linesep}{linesep}{1}{linesep}".format(header, content, linesep=os.linesep)


class ArgumentParser:
    @classmethod
    def parse_input(cls, argv, training_data_path_required=False, sample_information_file_path_required=False):
        """
        Parse command line input.
        :param argv: given arguments
        :param training_data_path_required: if the training data path is required
        :param sample_information_file_path_required: if the sample information file path is required
        :return: parsed arguments
        """
        parser = cls.create_argument_parser(sample_information_file_path_required, training_data_path_required)
        args = parser.parse_args(argv)
        return args

    @classmethod
    def create_argument_parser(cls, sample_information_file_path_required, training_data_path_required):
        """
        Method creating an argument parser
        :param sample_information_file_path_required: if the sample information file path is required
        :param training_data_path_required: if the training data path is required
        :return: parser
        """
        parser = argparse.ArgumentParser(description="Train machine learning model",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('-t', '--training_data_directory', type=cls.is_readable_dir,
                            required=training_data_path_required,
                            default=None, help="Give a path for the directory where subject folders are stored")
        parser.add_argument('-i', '--sample_information_file_path', type=cls.is_readable_file,
                            required=sample_information_file_path_required,
                            default=None, help="Specify a CSV file that contains subject folder names"
                                               + " (at column 1) and labels (at column number 4)")
        parser.add_argument('-o', '--output_table', default=None, required=False, type=cls.is_writable_location,
                            help="Specify the path for the output CSV file")
        parser.add_argument('-m', '--model_path', type=cls.is_writable_location,
                            default=None, required=False,
                            help="The path where the input model should be stored")
        parser.add_argument('-n', '--new_subject_path', type=cls.is_readable_dir,
                            default=None, help="A path to a subject that needs to be classified")
        parser.add_argument('-r', '--repeats', default=1, required=False, type=int,
                            help="The number of times to repeat cross validation for a reliable score")
        return parser

    @classmethod
    def is_readable_dir(cls, train_directory):
        """
        Checks whether the given directory is readable
        :param train_directory: a path to a directory in string format
        :return: train_directory
        :raises: Exception: if the given path is invalid
        :raises: Exception: if the given directory is not accessible
        """
        if not os.path.isdir(train_directory):
            raise Exception("directory: {0} is not a valid path".format(train_directory))
        if os.access(train_directory, os.R_OK):
            return train_directory
        else:
            raise Exception("directory: {0} is not a readable dir".format(train_directory))

    @classmethod
    def is_readable_file(cls, sample_information_file_path):
        """
        Checks whether the given directory is readable
        :param sample_information_file_path: a path to a file in string format
        :return: sample_information_file_path
        :raises: Exception: if the given path is invalid
        :raises: Exception: if the given directory is not accessible
        """
        if not os.path.isfile(sample_information_file_path):
            raise Exception("file path:{0} is not a valid file path".format(sample_information_file_path))
        if os.access(sample_information_file_path, os.R_OK):
            return sample_information_file_path
        else:
            raise Exception("file path:{0} is not a readable file".format(sample_information_file_path))

    @classmethod
    def is_writable_location(cls, path):
        cls.is_readable_dir(os.path.dirname(path))
        return path


def get_estimator_instance():
    """
    Function supplying the sklearn estimator instance also known as the classifier in this context required for
    cross validation.
    :return: RandomForestClassifier object instance with 100 trainable trees
    """
    clf = RandomForestClassifier(n_estimators=100)
    return clf


def get_sample_information_table(sample_information_file_path):
    """
    Function processing the sample information file path to a numpy array.
    The file should have a comma as a delimiter.
    :param sample_information_file_path:
    :return: numpy array containing 5 columns
    The output is as follows:
    Column 0: NAME column with names as Unicode 12 character string
    Column 1: AGE column with ages as Unicode 3 character string
    Column 2: SEX column with sex as Unicode 2 character string
    Column 3: CONDITION_SIMPLIFIED column with conditions as Unicode 9 character string (could be 'PAT' or 'CON')
    Column 4: CONDITION column with conditions as SubjectType enum instance
    ('CONTROL', 'ATAXIA', 'DCD', 'PATIENT' or 'UNKNOWN')
    """
    # Define names of the array
    names = ["NAME", "AGE", "SEX", "CONDITION_SIMPLIFIED", "CONDITION"]
    # Convert file to the array
    sample_information_table = np.genfromtxt(sample_information_file_path, names=names, delimiter=',',
                                             converters={
                                                 0: lambda s: s.decode(),  # Decode the names
                                                 4: lambda t: SubjectType.from_str(t.decode('utf-8'))
                                             },
                                             dtype=('U12', 'U3', 'U2', 'U9', SubjectType))
    # Return table / array
    return sample_information_table


def can_predict(args):
    """
    Function that checks whether it is possible to predict a subject given the set of arguments that are supplied
    :param args: the output from the argument parser
    :return: False if argument 'model_path' or 'new_subject_path' equals default value None
    and was thus not given, True if otherwise
    """
    # Return boolean
    return bool(args.model_path) and bool(args.new_subject_path)


def can_evaluate(args):
    """
    Function that checks whether it is possible to evaluate a classifier given the set of arguments that are supplied
    :param args: the output from the argument parser
    :return: False if argument 'training_data_directory' or 'sample_information_file_path' equals default value None
    and was thus not given, True if otherwise
    """
    # Return boolean
    return bool(args.training_data_directory) and bool(args.sample_information_file_path)


def can_train(args):
    """
    Function that checks whether it is possible to train and write the trained model to a given file path
    :param args: the output from the argument parser
    :return: False if argument 'model_path' equals default value None and was thus not given, True if otherwise
    """
    # Return args.model_path converted to a boolean
    return bool(args.model_path)


def get_cross_validation_indices():
    """
    Function supplying the cross validation indices required for cross validation
    :return: LeaveOneOut object instance
    """
    # Initialize LeaveOneOut cross validation instance and return it
    cross_validation_indices = LeaveOneOut()
    return cross_validation_indices


def main(argv=None):
    if argv is None:
        argv = sys.argv
    args = ArgumentParser.parse_input(argv[1:])
    # Get decision tree classifier
    clf = get_estimator_instance()

    if can_evaluate(args):
        # Obtain sample information
        sample_information_table = get_sample_information_table(args.sample_information_file_path)
        machine = Machine(args.training_data_directory, sample_information_table)
        # Evaluate the classifier with the data in args
        machine.evaluate(clf, get_cross_validation_indices(), args.repeats)
        # Process output options
        if args.output_table:
            machine.save_output_csv(args.output_table)
        # If model path is supplied
        if can_train(args):
            # Train estimator
            machine.train(clf)
            joblib.dump(machine, args.model_path)

    if can_predict(args):
        machine = joblib.load(args.model_path)
        subject = Subject.create_subject(args.new_subject_path)
        print(machine.predict_subject(subject))
    return 0


if __name__ == "__main__":
    sys.exit(main())
