#!/usr/bin/env python3

"""
Script to be used for plotting of the cumulative power spectrum
"""

import sys

import numpy as np
from matplotlib import pyplot as plt
from src.machine import ArgumentParser, Machine, get_sample_information_table
from src.subject import SubjectType


class CumulativePowerSpectrumPlotter:
    def __init__(self, subjects, max_range_value):
        self.max_range_value = max_range_value
        self.subjects = subjects

    def range01(self, x):
        """
        Changes the range of a given dataset between zero and one on the y axis
        :param x: a range
        :return: The new range of the data
        """
        return (x - np.min(x)) / (np.max(x) - np.min(x))

    def get_frequency_points(self, power_spectrum):
        points = [i for i in range(len(power_spectrum))]
        frequency_points = []
        for i in points:
            frequency_points.append(i / (len(power_spectrum) * 0.01953125))
        return frequency_points

    def plot_power_spectrum(self, arm, line_color):
        """
        Plots the power spectrum of the data set
        :param arm: Arm object instance
        :param line_color:
        :return: None
        """
        arm_power_spectrum = arm.hand_trajectory.power_spectrum_3d
        freq = self.get_frequency_points(arm_power_spectrum)
        freq = [i for i in freq if i < self.max_range_value]
        line, = plt.plot(freq, self.range01(np.cumsum(arm_power_spectrum[0:len(freq)])), color=line_color, ls='--',
                         alpha=0.64)
        return line

    def plot_power_spectra(self):
        """
        Plots multiple power spectra
        :return: None
        """
        Al = Ar = Dl = Dr = Cl = Cr = None
        for index, subject in enumerate(self.subjects):
            if subject.subject_type in (SubjectType.ATAXIA, SubjectType.PATIENT):
                # Plot ataxia patients or generic patient in two purple like colours
                Al = self.plot_power_spectrum(subject.left_arm, "purple")
                Ar = self.plot_power_spectrum(subject.right_arm, "violet")
            elif subject.subject_type == SubjectType.DCD:
                # Plot DCD patients in two red like colours
                Dl = self.plot_power_spectrum(subject.left_arm, "black")
                Dr = self.plot_power_spectrum(subject.right_arm, "gray")
            else:
                # Plot other (control) subjects in cyan like colours
                Cl = self.plot_power_spectrum(subject.left_arm, "cyan")
                Cr = self.plot_power_spectrum(subject.right_arm, "darkcyan")
        # Show plot
        plt.xlabel("frequency")
        plt.ylabel("normalized power")
        plt.legend((Al, Ar, Dl, Dr, Cl, Cr), (
            "Ataxia, left arm", "Ataxia, right arm", "DCD, left arm", "DCD, right arm", "Control, left arm",
            "Control, right arm"), loc='upper right')
        plt.show()
        return


def main(argv=None):
    if argv is None:
        argv = sys.argv
    parser = ArgumentParser.create_argument_parser(
        training_data_path_required=True,
        sample_information_file_path_required=True)
    parser.add_argument('--max_range', type=float, required=False, default=1.5, help=
                        "The maximum of the range of the plot")
    args = parser.parse_args(argv[1:])
    # Obtain sample information
    sample_information_table = get_sample_information_table(args.sample_information_file_path)
    machine = Machine(args.training_data_directory, sample_information_table)
    # Plot power spectra for every subject
    plotter = CumulativePowerSpectrumPlotter(machine.subjects, args.max_range)
    plotter.plot_power_spectra()
    return 0


if __name__ == "__main__":
    sys.exit(main())
