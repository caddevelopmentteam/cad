#!/usr/bin/env python3

import os
import sys

from src.reader import Reader


class Arm:
    def __init__(self, hand_trajectory, forearm_trajectory, upper_arm_trajectory, wrist_rotation, elbow_rotation):
        self.wrist_rotation = wrist_rotation
        self.hand_trajectory = hand_trajectory
        self.upper_arm_trajectory = upper_arm_trajectory
        self.elbow_rotation = elbow_rotation
        self.forearm_trajectory = forearm_trajectory
        return

    @classmethod
    def create_arm(cls, arm_path):
        """
        Constructs a subject using a given directory
        :param arm_path: Str, a directory containing coord files and from an arm
        :return: Arm, an arm instance
        """
        coord_file_options = ("redCoords.dat",
                              "brownCoords.dat",
                              "orangeCoords.dat",
                              "greenCoords.dat",
                              "purpleCoords.dat")
        trajectories = list()
        for filename in coord_file_options:
            try:
                trajectories.append(Reader.parse_file(os.path.join(arm_path, filename)))
            except FileNotFoundError as e:
                raise ArmFileNotFoundError("'{}' could not be found in {}".format(filename, arm_path))
        return Arm(*trajectories)

    def get_features(self):
        """
        Collects features from the hand movement and returns them
        :return: hand movement features
        """
        features = dict()
        for key, value in self.hand_trajectory.get_features().items():
            features["hand.{}".format(key)] = value
        for key, value in self.upper_arm_trajectory.get_features().items():
            features["upper_arm.{}".format(key)] = value
        for key, value in self.forearm_trajectory.get_features().items():
            features["forearm.{}".format(key)] = value
        return features


class ArmFileNotFoundError(OSError):
    pass


def main(argv=None):
    if argv is None:
        argv = sys.argv
    return 0


if __name__ == '__main__':
    sys.exit(main())
