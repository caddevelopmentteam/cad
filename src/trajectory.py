#!/usr/bin/env python3

import sys
import numpy as np
from enum import Enum
from scipy import stats
from scipy import signal
from sklearn.decomposition import PCA
from collections import namedtuple
import peakutils
import matplotlib.pyplot as plt
import copy


class Trajectory:
    """
    This class contains functions to calculate and store the characteristics to measure ataxia
    """
    t = 0.01953125

    def __init__(self, trajectory_array, sensor):
        # Implement test that tests if the trajectory array meets our requirements
        self.sensor = sensor
        self.trajectory_array = np.array([])
        self.set_trajectory_array(trajectory_array)
        self.duration = self.calculate_duration()
        self.trajectory_length = self.get_trajectory_length()
        self.fourier_transformation = self.calculate_fourier_transformation()
        self.power_spectrum_3d = self.get_power_spectrum_3d()
        self.frequency_points = self.calculate_frequency_points()
        self.slope = self.calculate_slope()
        self.AUC = self.calculate_AUC_trapz()

        # Calculate independent jerk
        independent_jerk = self.calculate_independent_jerk()
        self.jerk_statistics = stats.describe(independent_jerk)

        # Calculate residuals from filtered data
        sg_filtered_offset = self.get_residuals_from_sg_filter()
        self.residuals_statistics = stats.describe(sg_filtered_offset)

        # Extract features from the peaks
        self.trajectory_signal = None
        extremities = self.find_the_peaks()
        peak_to_peak_measurements = self.calculate_distances_summit_valley(extremities)
        self.peaks_dY_statistics = stats.describe(peak_to_peak_measurements[0])
        self.deviance_statistics = stats.describe(self.get_deviance_from_straight_movement(extremities))
        return

    def set_trajectory_array(self, array):
        """
        This is a method that tests if the array meet the requirements for a trajectory array
        :param: array: numpy.ndarray
        """
        if str(type(array)) != "<class 'numpy.ndarray'>":
            raise TrajectoryArrayTypeException("Expected <class 'numpy.ndarray'> and not " + str(type(array)))
        if array.ndim != 2:
            raise TrajectoryArrayShapeException("The array is not a 2D array")
        if array.shape[1] != 3:
            raise TrajectoryArrayShapeException("The 2D array does not contain 3 columns")
        self.trajectory_array = array
        return

    def calculate_duration(self):
        """
        Calculates the duration of the movement
        :return: duration(float)
        """
        duration = len(self.trajectory_array) * self.t
        return duration

    def calculate_fourier_transformation(self):
        """
        Performs a multidimensional fourier transformation on the data
        :return:
        """
        return np.fft.fftn(self.trajectory_array, axes=([0]))

    def get_power_spectrum(self, d):
        """
        :param d: dimension
        :return: power spectrum of fourier transformation
        """
        power_spectrum = np.real(self.fourier_transformation[:, d]) ** 2 + np.imag(
            self.fourier_transformation[:, d]) ** 2
        power_spectrum = np.delete(power_spectrum, 0, 0)
        return power_spectrum

    def get_power_spectrum_3d(self):
        """
        from matplotlib.mlab import PCA
        :return: power_spectrum_3d the powerspectrum of x y and z combined
        """
        power_spectrum_x = self.get_power_spectrum(0)
        power_spectrum_y = self.get_power_spectrum(1)
        power_spectrum_z = self.get_power_spectrum(2)
        power_spectrum_3d = power_spectrum_x + power_spectrum_y + power_spectrum_z
        return power_spectrum_3d

    def range01(self, x):
        """
        Changes the range of the input between 0 and 1 on the y axis
        :param x:
        :return: The new range between 0 and 1
        """
        return (x - np.min(x)) / (np.max(x) - np.min(x))

    def calculate_frequency_points(self):
        points = [i for i in range(len(self.power_spectrum_3d))]
        frequency_points = []
        for i in points:
            frequency_points.append(i / (len(self.power_spectrum_3d) * self.t))
        return frequency_points

    def plot_power_spectrum(self):
        """
        plots the power spectrum
        :return: None
        """
        plt.scatter(self.frequency_points, self.range01(self.power_spectrum_3d))
        plt.show()
        return

    def calculate_slope(self):
        """
        Calculates the slope of the data
        :return: slope the slope of a line fitted on the data signal
        """
        freq = [i for i in self.frequency_points if i < 0.4]
        slope, intercept = np.polyfit(self.range01(np.cumsum(self.power_spectrum_3d[0:len(freq)])), freq, 1)
        return slope

    def calculate_jerk(self, distances):
        """
        Calculates the jerk (change in acceleration over time) from the trajectory array
        :type distances: numpy.ndarray
        :param distances: array with changes in position
        :rtype: numpy.ndarray
        :return: jerk
        """
        # Calculate speed by dividing by time
        speed = distances / self.t
        # Calculate acceleration by taking difference between consecutive rows and dividing by time
        acceleration = np.diff(speed) / self.t
        # Calculate jerk by taking difference between consecutive rows and dividing by time
        jerk = np.diff(acceleration) / self.t
        return jerk

    def calculate_independent_jerk(self):
        """
        Calculates jerk for all three dimensions and returns the magnitude
        :return: magnitude of jerk
        """
        jerk_list = list()
        for i in range(0, 2):
            jerk_list.append(self.calculate_jerk(self.smooth_trajectory_dim(self.trajectory_array[:, i])))
        # Return magnitude of these jerk vectors
        jerk_magnitude = np.linalg.norm(np.asarray(jerk_list), axis=0)
        return jerk_magnitude

    def calculate_distances(self):
        """
        Calculates the change in position from all coordinates.
        This is done by taking the difference from consecutive rows and calculating the norm
        :rtype: numpy.ndarray
        :return: change in position for input coordinates
        """
        xyz_distances = np.diff(self.trajectory_array, axis=0)
        # Calculate the pythagoras for dx, dy > dxdy, and for dxdy, dz > dxdydz(d)
        return np.linalg.norm(xyz_distances, axis=1)

    def smooth_trajectory_dim(self, arr, window=17, polyorder=3):
        """
        smooths the given array using the savitzky-golay filter with a window set to 17 and a polyorder set to three
        :param arr: a 1d numpy array representing a trajectory
        :rtype: numpy.ndarray
        :return: smoothed trajectory
        """
        return signal.savgol_filter(arr, window, polyorder)

    def get_trajectory_length(self):
        """
        calculates the distances between consecutive coordinates and adds them up
        :return: The length of the entire trajectory
        """
        return sum(self.calculate_distances())

    def get_residuals_from_sg_filter(self):
        """
        calculates the residuals / fit deviations from a fit. The fit is the output of a
        savitzky-golay filter with the window set to 73 and the polyorder set to 3
        :return: square root of the sum of squares of residuals
        """
        residuals = list()
        # Loop through columns of the trajectory data
        for i in range(0, 2):
            # Get residuals from smoothed trajectory
            residuals.append(
                np.absolute(self.smooth_trajectory_dim(self.trajectory_array[:, i], 73, 3) -
                            self.trajectory_array[:, i]))
        # Return magnitude of these residual vectors
        residuals_magnitude = np.linalg.norm(np.asarray(residuals), axis=0)
        return residuals_magnitude

    def get_features(self):
        """
        Collects instance variables in a dictionary
        :return: dictionary containing values that can be used for classification
        """
        # Initialize dictionary for storing features
        features = dict()
        # The following attributes have to be omitted from the feature dict; these are arrays and/or irrelevant
        attributes_to_omit_from_feature_dict = ["trajectory_array", "fourier_transformation", "power_spectrum_3d",
                                                "sensor", "frequency_points", "trajectory_signal", "extremities"]
        # Loop through attributes in self and store the attributes used for classification
        for key, value in self.__dict__.items():
            # Find the attributes to omit from the feature dict, these are arrays and/or irrelevant
            if key in attributes_to_omit_from_feature_dict:
                # Don't add the these attributes, these are arrays and/or irrelevant
                pass
            # The following statistics are useful and should be used in classification,
            # however, the statistics are stored in scipy DescribeResults instances
            elif key in ["jerk_statistics", "residuals_statistics", "peaks_dT_statistics", "peaks_dY_statistics",
                         "deviance_statistics"]:
                # Unpack the object
                self.unpack_statistics_from_object(features, key, value)
            else:
                # Save any other values
                features[key] = value
        # Return the features through the dictionary signal
        return features

    def unpack_statistics_from_object(self, features, key, value):
        """
        Unpacks scipy DescribeResult instance
        :param features: dictionary for storing features
        :param key: name of attribute
        :param value: DescribeResult instance
        """
        # Unpack scipy DescribeResult instance
        # For the variable name: only use the part before the underscore ('_')
        # This can be 'jerk' or 'residuals'
        measure = key
        # Store the min and max values that are available in the minmax attribute via a tuple (min, max)
        # Get the min value from the tuple
        features[measure + "_min"] = value.minmax[0]
        # Get the max value from the tuple
        features[measure + "_max"] = value.minmax[1]
        # Store other attributes from the DescribeResults instance
        features[measure + "_mean"] = value.mean
        features[measure + "_variance"] = value.variance
        features[measure + "_skewness"] = value.skewness
        features[measure + "_kurtosis"] = value.kurtosis

    def calculate_AUC_trapz(self):
        """
        Calculates the AUC
        :return: AUC the area under the curve of the data of frequencies between zero and 0.4
        """
        freq = [i for i in self.frequency_points if i < 0.4]
        AUC = np.trapz(self.range01(np.cumsum(self.power_spectrum_3d[0:len(freq)])), freq)
        return AUC

    def find_the_peaks(self):
        """
        finds the peaks in the data set
        :return:
        """
        pca = PCA(n_components=1)
        principal_components = pca.fit_transform(self.trajectory_array)
        pca1 = principal_components[:, 0]
        # performs pca with one components on the data set
        # Calculates the summits in the data
        max_indexes = peakutils.indexes(pca1, thres=0.2, min_dist=30)
        # Calculates the valleys in the data
        min_array = pca1 * -1
        min_indexes = peakutils.indexes(min_array, thres=0.2, min_dist=30)

        # Get extremities
        self.trajectory_signal = pca1
        return self.get_extremities(list(min_indexes), list(max_indexes))

    def trim_signal(self, trajectory_signal):
        """
        Trims the signal to try and delete the waiting a participant might do before starting the exercise.
        The signal is trimmed until a two summits are encountered interrupted by a valley, or two valleys are
        encountered interrupted by a summit.
        :param trajectory_signal: a trajectory signal
        :param extremities: a list of indices with extremity type in a tuple that represent valleys in the signal
        """
        # Initialize storage variables for first and second extremity types
        first_extremity_type = None
        second_extremity_type = None
        trim_start_index = 0
        # Loop through every extremity
        extremities_new = list()
        for i, extremity in enumerate(self.extremities):
            # Start by storing the first extremity type
            if first_extremity_type is None:
                first_extremity_type = extremity.extremity_type
            # Store second extremity type if this is encountered
            elif first_extremity_type != extremity.extremity_type and second_extremity_type is None:
                second_extremity_type = extremity.extremity_type
            # If the first extremity type is encountered after the second type has been set: get index
            elif second_extremity_type is not None and first_extremity_type == extremity.extremity_type \
                    and trim_start_index == 0:
                trim_start_index = extremity.index
            elif trim_start_index != 0:
                # Add trim_start_index to indices of remaining extremities
                extremities_i_ = copy.copy(self.extremities[i])
                extremities_i_ = extremities_i_._replace(index=extremities_i_.index - trim_start_index)
                extremities_new.append(extremities_i_)
            else:
                # Remove extremities that will be prior to new starting index
                pass
        if len(list(filter(lambda e: e.extremity_type == ExtremityType.SUMMIT, extremities_new))) == 0 or len(
                list(filter(lambda e: e.extremity_type == ExtremityType.VALLEY, extremities_new))) == 0:
            return trajectory_signal
        self.extremities = extremities_new
        return trajectory_signal[trim_start_index:]

    def get_extremities(self, min_indexes, max_indexes):
        """
        Combine extremities from
        :param max_indexes: a list of indices that represent a summit
        :param min_indexes: a list of indices that represent a valley
        :return:
        """
        # Declare named tuple for extremities
        Extremity = namedtuple('Extremity', ['index', 'extremity_type'])
        # Initialize new list
        c = list()
        # Loop through min and max indices until one of the lists is empty
        while min_indexes and max_indexes:
            # Check if the last index from valleys is bigger than the last index of summits
            if min_indexes[len(min_indexes) - 1] > max_indexes[len(max_indexes) - 1]:
                # Index from the last valley is the biggest:
                # Pop it and append it to the new extremities list together with the extremity type
                c.append(Extremity(min_indexes.pop(), ExtremityType.VALLEY))
            else:
                # Index from the last summit is the biggest:
                # Pop it and append it to the new extremities list together with the extremity type
                c.append(Extremity(max_indexes.pop(), ExtremityType.SUMMIT))
        # When one of the indices is empty: extend the new list with the remaining summits or valleys
        c.extend([Extremity(index, ExtremityType.VALLEY) for index in min_indexes[::-1]])
        c.extend([Extremity(index, ExtremityType.SUMMIT) for index in max_indexes[::-1]])
        # Return the reversed list (ascending)
        return c[::-1]

    def get_deviance_from_straight_movement(self, extremities):
        """
        Get deviance between the signal and the extremities
        :param extremities:
        :return:
        """
        # Initialize deviance list
        deviances = np.asarray([])
        # Declare first_extremity variable
        first_extremity = None
        # Loop through extremities
        for second_extremity in extremities:
            if first_extremity:
                # Get real segment through indexing
                real_segment = np.asarray(self.trajectory_signal[first_extremity.index:second_extremity.index])
                # Calculate the amount of samples the real segment consists
                sample_distance = second_extremity.index - first_extremity.index
                # Get the ideal segment between the two extremities
                straight_segment = np.linspace(self.trajectory_signal[first_extremity.index],
                                               self.trajectory_signal[second_extremity.index],
                                               sample_distance)
                # Get deviance
                absolute = np.absolute(real_segment - straight_segment)
                # Collect deviances
                deviances = np.append(deviances, absolute)
            first_extremity = second_extremity
        return deviances

    def calculate_distances_summit_valley(self, extremities):
        """
        Calculates the width and height between the extremes
        :param extremities:
        :return:
        The description of heights and widths
        """
        heights = list()
        widths = list()
        last_extremity = None
        for extremity in extremities:
            if last_extremity:
                # checks if the extremity type is different
                if extremity.extremity_type != last_extremity.extremity_type:
                    # Calculates the dY (the height)
                    absolute_dY = abs(
                        self.trajectory_signal[last_extremity.index] - self.trajectory_signal[extremity.index])
                    heights.append(absolute_dY)
                    # Calculates the dT ( the width)
                    absolute_dT = abs(
                        last_extremity.index - extremity.index)
                    widths.append(absolute_dT)
                    last_extremity = extremity
            else:
                last_extremity = extremity
        return heights, widths


class Sensor(Enum):
    HAND = 1
    FOREARM = 2
    UPPER_ARM = 3
    ELBOW = 4
    WRIST = 5


class ExtremityType(Enum):
    SUMMIT = 1
    VALLEY = 2


class TrajectoryArrayShapeException(Exception):
    pass


class TrajectoryArrayTypeException(Exception):
    pass


def main(argv=None):
    if argv is None:
        argv = sys.argv
    return 0


if __name__ == '__main__':
    sys.exit(main())
