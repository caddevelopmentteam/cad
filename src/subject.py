#!/usr/bin/env python3

import os
import re as regex
import sys
from enum import Enum

from src.arm import Arm


def get_matching_directory(entries, expression):
    matches = list(filter(lambda entry: regex.fullmatch(expression, entry), entries))
    if len(matches) < 1:
        raise DirNotFoundError("Could not find a directory that fully matches {}".format(expression))
    if len(matches) > 1:
        raise DirNotFoundError(
            "Found {} directories that fully match {}, but expected one".format(len(matches), expression))
    return matches[0]


class Subject:
    def __init__(self, left_arm, right_arm, subject_type, name):
        self.name = name
        self.left_arm = left_arm
        self.right_arm = right_arm
        self.subject_type = subject_type
        return

    @classmethod
    def create_subject(cls, subject_path):
        """
        Constructs a subject using a given directory
        :param subject_path: Str, a directory containing coord files and from a left and right hand
        :return: Subject, a subject instance
        """
        entries = os.listdir(subject_path)
        try:
            # Fetch both arms from the given directory.
            # The directory for an should look like the following:
            # "S<DIGITS><L|R>"
            # Any number can be in the center section.
            left_arm = cls.fetch_arm_from_directory(entries, subject_path, r"S\d+L")
            right_arm = cls.fetch_arm_from_directory(entries, subject_path, r"S\d+R")
            subject_type = cls.determine_simple_subject_type(subject_path)
            return Subject(left_arm, right_arm, subject_type, os.path.basename(subject_path))
        except DirNotFoundError as e:
            raise DirNotFoundError("{} in subject folder {}".format(str(e), subject_path))

    @classmethod
    def fetch_arm_from_directory(cls, entries, subject_path, directory_regex):
        """
        Returns an Arm instance located in the given path and by using the given regex to match the directory
        :param entries: Subdirectories in the subject_path
        :param subject_path: Directory where the arms are located
        :param directory_regex: Regular expression that should match a directory
        :return: Arm instance
        """
        dir = get_matching_directory(entries, directory_regex)
        arm_path = os.path.join(subject_path, dir)
        return Arm.create_arm(arm_path)

    @classmethod
    def determine_simple_subject_type(cls, subject_path):
        """
        Gets the subject type given the path of a subject.
        Subject type is 'Patient' if the string 'PAT' is in the dir of the subject.
        Subject type is 'Control' if else.
        :param subject_path: path where the subjects details are located
        :return:
        """
        subject_type = SubjectType.CONTROL
        if 'PAT' in os.path.split(subject_path)[1]:
            subject_type = SubjectType.PATIENT
        return subject_type

    def is_subject_patient(self):
        """
        Returns true if the subjects type equals the subject type of ataxia
        :return: true or false
        """
        return self.subject_type == SubjectType.ATAXIA or self.subject_type == SubjectType.DCD

    def get_np_feature_array(self):
        """
        Collects the features of both the left and right arm with the comparable features
        distributed with a max or min prefix depending on the value.
        :return: features
        """
        features = dict()
        for key, value in self.right_arm.get_features().items():
            o_value = self.left_arm.get_features()[key]
            features['max.{}'.format(key)] = max(value, o_value)
            features['min.{}'.format(key)] = min(value, o_value)
        return features


class DirNotFoundError(OSError):
    pass


class SubjectType(Enum):
    """
    Enum representing the condition of a subject
    """
    CONTROL = 1
    ATAXIA = 2
    DCD = 3
    UNKNOWN = 4
    PATIENT = 5

    @staticmethod
    def from_str(label):
        """
        Static method returning the SubjectType corresponding to the given label string
        :param label: string that spells either ataxia, control or dcd in lower- or uppercase
        :return: the corresponding SubjectType
        """
        if label in ('ATAXIA', 'Ataxia', 'ataxia'):
            return SubjectType.ATAXIA
        elif label in ('CONTROL', 'Control', 'control'):
            return SubjectType.CONTROL
        elif label in ('DCD', 'dcd'):
            return SubjectType.DCD
        else:
            return SubjectType.UNKNOWN


def main(argv=None):
    if argv is None:
        argv = sys.argv
    var = Subject.create_subject(argv[1])
    return 0


if __name__ == '__main__':
    sys.exit(main())
