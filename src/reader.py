#!/usr/bin/env python3

import ntpath
import sys

import numpy as np

from src.trajectory import Trajectory, Sensor


class Reader:
    coord_file_options = {"brownCoords.dat": Sensor.FOREARM,
                          "greenCoords.dat": Sensor.WRIST,
                          "orangeCoords.dat": Sensor.UPPER_ARM,
                          "purpleCoords.dat": Sensor.ELBOW,
                          "redCoords.dat": Sensor.HAND}

    @classmethod
    def read_file(cls, opened_file):
        """
        Converts an open file to a numpy matrix.
        :param opened_file: an open file that has 4 columns the last three of which are numeric
        :return: a numpy matrix containing the last three columns of the given opened file
        """
        replace_commas = lambda x: x.replace(b',', b'.')
        return np.loadtxt(opened_file, converters={1: replace_commas, 2: replace_commas, 3: replace_commas},
                          usecols=(1, 2, 3))

    @classmethod
    def parse_file(cls, path):
        """
        Parses a file found on the given path to a numpy array
        :param path: str, path where a file is located
        :return: numpy array
        """
        basename = ntpath.basename(path)
        with open(path) as opened_file:
            trajectory = Trajectory(cls.read_file(opened_file), cls.coord_file_options[basename])
        return trajectory


def main(argv=None):
    if argv is None:
        argv = sys.argv
    if len(argv) > 1:
        my_trajectory = Reader.parse_file(argv[1])
        print(my_trajectory.trajectory_array.shape)
        print(my_trajectory.duration)
        print(my_trajectory.fourier_transformation)
    return 0


if __name__ == "__main__":
    sys.exit(main())
