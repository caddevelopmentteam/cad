#!/usr/bin/env python3

"""
Script to be used for the optimization of parameters
"""

import sys

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import LeaveOneOut

from src.machine import ArgumentParser, Machine, get_sample_information_table


class ParameterEstimator:
    """
    Object that is responsible for estimating and evaluating parameters
    """
    # Define some search ranges for various attributes
    parameter_search_ranges = {'max_features': range(3, 10, 1),
                               'max_depth': range(2, 10, 1),
                               'min_samples_split': np.arange(0.001, 0.002, 0.0001),
                               'min_samples_leaf': np.arange(0.18, 0.20, 0.001),
                               'max_leaf_nodes': range(120, 140, 1),
                               'min_impurity_split': np.arange(0.18, 0.20, 0.001)}
    scoring_metrics = 'accuracy'

    def __init__(self):
        """
        Constructor method
        """
        # Get LOO cross validation indexes
        loo = LeaveOneOut()
        # Setup grid search
        self.grid_search_fitter = GridSearchCV(RandomForestClassifier(n_estimators=100),
                                               param_grid=self.parameter_search_ranges,
                                               scoring=self.scoring_metrics,
                                               cv=loo, refit='AUC', n_jobs=4, verbose=0)

    def grid_search(self, X, y):
        """
        Method performing grid search
        :param X: Attributes array
        :param y: True labels array
        """
        self.grid_search_fitter.fit(X, y)
        print("Best parameters set found on development set:")
        print()
        print(self.grid_search_fitter.best_params_)
        print()
        print("Grid scores on development set:")
        print()
        means = self.grid_search_fitter.cv_results_['mean_test_score']
        stds = self.grid_search_fitter.cv_results_['std_test_score']
        for mean, std, params in zip(means, stds, self.grid_search_fitter.cv_results_['params']):
            print("%0.3f (+/-%0.03f) for %r"
                  % (mean, std * 2, params))
        print()


def main(argv=None):
    if argv is None:
        argv = sys.argv
    args = ArgumentParser.parse_input(argv[1:],
                                      training_data_path_required=True,
                                      sample_information_file_path_required=True)
    # Obtain sample information
    sample_information_table = get_sample_information_table(args.sample_information_file_path)
    machine = Machine(args.training_data_directory, sample_information_table)
    # Evaluate the classifier with the data in args
    ParameterEstimator().grid_search(machine._feature_array, machine._label_array)
    # Process output options
    return 0


if __name__ == "__main__":
    sys.exit(main())
